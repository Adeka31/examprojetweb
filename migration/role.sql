CREATE TABLE IF NOT EXISTS `role` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `role` (`id`, `name`) VALUES
    (1,'admin'),
    (2, 'teacher'),
    (3, 'student'),
    (4, 'guest'),
    (5, 'banished');