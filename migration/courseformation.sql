CREATE TABLE IF NOT EXISTS `formation_course` (
    `id` bigint(20) UNSIGNED NOT NULL,
    `formationid` bigint(20) UNSIGNED NOT NULL,
    `courseid` bigint(20) UNSIGNED NOT NULL,
    `period` tinyint(3) UNSIGNED NOT NULL,
    `determinant` tinyint(1) UNSIGNED NOT NULL,
    `prereq` bigint(20) UNSIGNED NOT NULL,
    `teacher` bigint(20) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `formationid` (`formationid`, `courseid`),
    KEY `formationid` (`formationid`),
    KEY `courseid` (`courseid`),
    KEY `teacher` (`teacher`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;