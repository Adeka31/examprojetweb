<?php

namespace app\Helpers;

use app\Controllers\Color;
use app\Controllers\Course;
use stdClass;

class Bootstrap
{
    /**
     * Affichage d'un tableau HTML sur base d'un objet ou d'un array de données
     *
     * @param array|object $data    les données à afficher
     * @param array|object $cols    l'entête des colonnes
     * @param string $title         l'éventuel titre du tableau
     * @param string $class         les éventuelles classes css du tableau
     * @return string
     */
    public static function table(array|object $data, array|object $cols, string $title = '', string $class = ''): string
    {
        $thead = '';
        $tbody = '';
        if (is_array($cols) || is_object($cols)) {
            foreach ($cols as $th) {
                $thead .= '<th>' . $th . '</th>';
            }
        } else {
            $thead = '<th>' . $cols . '</th>';
        }
        if (is_array($data) || is_object($data)) {
            foreach ($data as $sub) {
                $tbody .= '<tr>';
                if (is_array($sub) || is_object($sub)) {
                    foreach ($sub as $value) {
                        $tbody .= '<td>' . $value . '</td>';
                    }
                } else {
                    $tbody .= '<td>' . $sub . '</td>';
                }
                $tbody .= '</tr>';
            }
        } else {
            $tbody .= '<tr><td>' . $data . '</td></tr>';
        }

        return '<h2>' . $title . '</h2><table class="table table-striped ' . $class . '"><thead><tr>' . $thead . '</tr></thead><tbody>' . $tbody . '</tbody></table>';
    }

    /**
     * Récupère les options d'un select de formulaire HTML sur base d'un objet
     *
     * @param object|array $options
     * @param string $selected
     * @return string
     */
    public static function getFormOptions(object|array $options, string $selected = ''): string
    {
        $output = '';
        foreach ($options as $key => $value) {
            if ($key == $selected) {
                $attr = 'selected';
            } else {
                $attr = '';
            }
            $output .= '<option value="' . $key . '" ' . $attr . '>' . $value . '</option>';
        }
        return $output;
    }

    /**
     * Vue du profil utilisateur + création modal pour update
     *
     * Appelé par la méthode profile de la classe Bootstrap, qui est elle-même appelée par la méthode render de la classe Output
     *
     * @see Bootstrap::profile()
     * @see Output::render()
     * @param object $data      l'objet contenant les données à afficher dans le tableau
     * @param string $class     l'éventuelle classe css du tableau
     * @return string
     */
    public static function profile(object $data, string $class = ''): string
    {
        
        $tbody = '';
        $modal = '';
        $modalBody = '<form action="index.php?view=api/user/update" method="post" enctype="multipart/form-data">';
        foreach ($data as $key => $value) {
            $type = '';
            $modif = '';
            if ($key == 'image') {
                $type = 'file';
                $value = '<div class="row">
                            <div class="col-6">
                                <img src="' . $value . '?' . time() . '" alt="photo" class="d-block img-fluid" id="profile-photo">
                            </div>
                          </div>';
            }


            else if(in_array($key, ['login', 'firstname', 'surname', 'address'])){
                $type='text';
            }
            else if($key == 'email'){
                $type = 'email';
            }
            else if($key == 'password'){
                $type = 'password';
                $value ='';
                
            }
            else if($key == 'birthdate'){
                $type = 'date';
            }
            else if($key == 'phonenumber'){
                $type = 'number';
            }
            else if(in_array($key, ['created', 'lastlogin', 'updated', 'lang'])){
                $type = 'text';
                $modif = 'disabled';
            }
            else if($key == 'id'){
                $type = 'hidden';
                
            }


            if($key != 'id'){
                $modalBody .= '<label for="uu-' . $key . '">' . $key . '</label>';
                $tbody .= '<tr><th>' . Text::getStringFromKey($key) . '</th><td>' . $value . '</td></tr>';
            }
            $modalBody .= '<input type="' . $type . '" id="uu-' . $key . '" name="' . $key . '" class="form-control" ' . $modif . ' value="' . $value . '">';
            
        }

        $modalBody .= '<button id="btn_modal_update"  class="btn btn-sm btn-primary" onclick=""> Update </button> </form>';
                


        $modal .= self::viewModal('modalupdate', 'update', $modalBody, '', 'lg');
        return '<h2>' . Text::getStringFromKey('profile') . '</h2>
                <a href="index.php?view=api/user/exportProfile/' . $_SESSION['userid'] . '/true" class="btn btn-sm btn-primary">Exporter</a>
                <table class="table ' . $class . '">' . $tbody . '</table>
                <button id="btn_update"  class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#modalupdate" > Update </button>' .$modal
                ;

        
        
    }
/* onclick="getmodal()" */
    /**
     * @param object $data
     * @return void
     */
    public static function exportProfile(object $data): void
    {
        ob_clean();
        $filename = $data->login . '_' . time() . '.json';
        // Envoi des headers HTTP au browser pour le téléchargement du fichier.
        header('Content-type: application/json');
        header('Content-disposition: attachment; filename="' . $filename . '"');
        // output du contenu au format json
        //echo json_encode($data);
        
        if ($data->format == 'json'){

            unset($data->format);
            $j = json_encode($data, JSON_PRETTY_PRINT);
            echo $j;
            exit();

        } else {
            unset($data->format);
            foreach ($data as $key => $value){
                   echo $key .' : '. $value . "\n";  
            }
        }

        
    }


    public static function exportprofiletotxt(object $data): void
    {

          //php trick : convertir un tableau d'objets en tableau associatif (attention, c'est relativement lent comme process)
          $data = json_decode(json_encode($data), true);
          $filename = 'courses_list_' . time() . '.txt';
          // ouverture du flux (en mode écriture)
          $ressource = fopen($filename, 'w');
          foreach ($data as $course) {
              // ajout d'une ligne au format csv
              fputcsv($ressource, $course);
          }
          // fermeture du flux
          fclose($ressource);
  
        // Envoi des headers HTTP au browser pour le téléchargement du fichier.
        header('Content-Type: text/txt');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        // output du contenu du fichier créé
        echo file_get_contents($filename);
        // suppression du fichier créé sur le serveur
        unlink($filename);
    }


    /**
     * @param array $data
     * @return void
     */
    public static function exportCourseList(array $data): void
    {
        //php trick : convertir un tableau d'objets en tableau associatif (attention, c'est relativement lent comme process)
        $data = json_decode(json_encode($data), true);
        $filename = 'courses_list_' . time() . '.csv';
        // ouverture du flux (en mode écriture)
        $ressource = fopen($filename, 'w');
        foreach ($data as $course) {
            // ajout d'une ligne au format csv
            fputcsv($ressource, $course);
        }
        // fermeture du flux
        fclose($ressource);

        // Envoi des headers HTTP au browser pour le téléchargement du fichier.
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        // output du contenu du fichier créé
        echo file_get_contents($filename);
        // suppression du fichier créé sur le serveur
        unlink($filename);

        // alternative sans écriture sur le disque
//        header('Cache-Control: no-store');
//        $buffer = fopen('php://output', 'r+');
//        foreach ($data as $course) {
//            fputcsv($buffer, $course);
//        }

    }

    /**
     * Vue du formulaire de mise à jour du profil utilisateur
     *
     * @param object $data
     * @return string
     */
    public static function profileUpdate(object $data): string
    {
        $lang = new stdClass();
        $lang->en = 'English';
        $lang->fr = 'Français';

       
 
        return '<hr>
                 <h3><a href="#profile-update-collapse" data-bs-toggle="collapse" role="button" class="text-decoration-none">' . Text::getStringFromKey('update') . '</a></h3>
                 <div class="collapse" id="profile-update-collapse">
                    <form action="index.php?view=api/user/update" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="uu-userid" name="id" value="' . $data->id . '">
                        <label for="uu-password">' . Text::getStringFromKey('password') . '</label>
                        <input type="password" id="uu-password" name="password" class="form-control" placeholder="Pour changer votre mot de passe, cliquez ici">
                        <label for="uu-email">' . Text::getStringFromKey('email') . '</label>
                        <input type="email" id="uu-email" name="email" class="form-control" value="' . $data->email . '">
                        <label for="uu-lang">' . Text::getStringFromKey('lang') . '</label>
                        <select name="lang" id="uu-lang" class="form-control">
                            ' . self::getFormOptions($lang, $data->lang) . '
                        </select>
                        <label for="uu-photo">' . Text::getStringFromKey('photo') . '</label>
                        <p><input type="file" id="uu-photo" name="photo"></p>
                        <input type="submit" class="btn btn-primary" value="' . Text::getStringFromKey('submit') . '">
                    </form>
                 </div>';


        
    }

    /**
     * @param array $courses
     * @return string
     */
    public static function profileCourses(array $courses): string
    {
        $body = '';
        foreach ($courses as $row) {
            $body .= '<tr>';
            foreach ($row as $key => $value) {
                if ($key == 'det') {
                    $value = Text::yesOrNo($value);
                } elseif ($key == 'courseid') {
                    continue;
                }
                $body .= '<td>' . $value . '</td>';
            }
            $body .= '</tr>';
        }
        return '<hr><h2><a href="#profile-courses-list" data-bs-toggle="collapse" role="button" class="text-decoration-none">' . Text::getStringFromKey('courses') . '</a></h2>
                <table class="table table-striped collapse" id="profile-courses-list">
                    <thead>
                        <tr>
                            <th>' . Text::getString(['formation', 'formation']) . '</th>
                            <th>' . Text::getString(['course', 'cours']) . '</th>
                            <th>' . Text::getString(['periods', 'périodes']) . '</th>
                            <th>' . Text::getString(['determining', 'déterminant']) . '</th>
                            <th>' . Text::getString(['prerequisite', 'prérequis']) . '</th>
                            <th>' . Text::getString(['teacher', 'professeur']) . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>
                <a href="" class="btn btn-sm btn-primary">UP</a>';
    }

    /**
     * Vue de la liste des cours
     *
     * @link http://localhost/web/js/main.js
     * @param array $data
     * @return string
     */
    public static function courses(array $data): string
    {
        // déclaration de la variable, comme une string vide, destinée à contenir le body du tableau HTML
        $body = '';
        $modal = '';
        // boucles foreach imbriquées
        // la première parcourt le tableau d'objets $data afin d'en extraire chaque objet, correspondant à une ligne du tableau HTML ($row)
        foreach ($data as $row) {
            $course = new Course();
            // concaténation de la balise HTML <tr> dans la variable $body, représentant le début d'une ligne dans le tableau HTML
            $body .= '<tr>';
            // seconde boucle pacourant chaque objet du tableau d'objets $data
            foreach ($row as $key => $value) {
                // concaténation de chaque élément (propriété) de l'objet au sein d'une balise HTML <td> représentant une cellule du tableau HTML
                if ($key == 'det') {
                    $value = Text::yesOrNo($value);
                } elseif ($key == 'courseid') {
                    continue;
                }
                $body .= '<td>' . $value . '</td>';
            }
             if (!$course->getEnrol($row->courseid, $_SESSION['userid'])) {
                $body .= '<td><a href="index.php?view=api/course/enrol/' . $row->courseid . '/' . $_SESSION['userid'] . '" class="btn btn-sm btn-success">+</a></td>';
            } else {
                $body .= '<td>Déjà inscrit</td>';
            }
            $body .= '</tr>'; 
            // concaténation de la balise fermante HTML <tr> dans la variable $body, représentant la fin d'une ligne dans le tableau HTML
        }
        // la fonction retourne le code HTML complet (représentant la vue)
        // le tableau HTML reçoit diverses classes du Framework CSS Bootstrap, ainsi qu'une classe CSS custom "table-dt" associée à la variable de session "lang", formant ainsi 3 nouvelles classes CSS : table-dt, table-dten et table-dtfr
        // ces nouvelles classes CSS doivent être définies dans le projet, elles le sont dans le fichier js/main.js
        // la balise <table> reçoit un attribut id (unique) permettant au Javascript et au CSS de pouvoir le cibler facilement dans le DOM
        return '<h2>' . Text::getStringFromKey('courses') . '</h2>
                <table class="table table-striped table-dt' . $_SESSION['lang'] . '" id="courses-list">
                    <thead>
                        <tr>
                            <th>' . Text::getString(['formation', 'formation']) . '</th>
                            <th>' . Text::getString(['course', 'cours']) . '</th>
                            <th>' . Text::getString(['periods', 'périodes']) . '</th>
                            <th>' . Text::getString(['determining', 'déterminant']) . '</th>
                            <th>' . Text::getString(['prerequisite', 'prérequis']) . '</th>
                            <th>' . Text::getString(['teacher', 'professeur']) . '</th>
                            <th>' . Text::getString(['enrol', 'inscrire']) . '</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $body . '
                    </tbody>
                </table>' . $modal;

        // Alternative utilisant la méthode générique de la classe Bootstrap. Plus court à écrire, mais moins flexible et ne gérant pas les traductions.
        // return self::table($data, array_keys(get_object_vars($data[0])), 'Courses');
    }

    /**
     * Vue générique pour les messages dans une div utilisant la classe d'alerte bootstrap
     *
     * @see Output::render()
     * @param string $message
     * @param string $class     Débute par la classe Bootstrap associée à la couleur de l'alerte (danger, info, success, warning)
     * @return string
     */
    public static function messageBox(string $message, string $class = 'danger'): string
    {
        return '<div class="alert alert-' . $class . '">' . $message . '</div>';
    }

    /**
     * @param string $content
     * @param string $type
     * @param bool $dismiss
     * @return string
     */
    public static function alert(string $content, string $type = 'success', bool $dismiss = false): string
    {
        if ($dismiss) {
            return '<div class="alert alert-' . $type . ' alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              ' . $content . '
            </div>';
        } else {
            return '<div class="alert alert-' . $type . '">' . $content . '</div>';
        }
    }


    /**
     * @param string $link
     * @param string $caption
     * @param string $class
     * @return string
     */
    public static function linkBtn(string $link, string $caption, string $class = 'default'): string
    {
        return '<a href="' . $link . '" class="btn btn-' . $class . '">' . $caption . '</a>';
    }

    /**
     * @param string $link
     * @param string $caption
     * @param string $target
     * @return string
     */
    public static function link(string $link, string $caption, string $target = 'default'): string
    {
        return '<a href="' . $link . '" class="lien" target="' . $target . '">' . $caption . '</a>';
    }

    /**
     * Bouton Modal bootstrap
     *
     * @param string $target_id
     * @param string $btn_text
     * @param string $btn_class
     * @return string
     */
    public static function btnModal(string $target_id, string $btn_text, string $btn_class = 'primary'): string
    {
        return '<button type="button" class="btn btn-' . $btn_class . '" data-bs-toggle="modal" data-bs-target="#' . $target_id . '">' . $btn_text . '</button>';
    }

    /**
     * Link Modal bootstrap
     *
     * @param string $target_id
     * @param string $caption
     * @param string $class
     * @return string
     */
    public static function linkModal(string $target_id, string $caption, string $class = ''): string
    {
        return '<a href="#" class="link ' . $class . '" data-bs-toggle="modal" data-bs-target="#' . $target_id . '">' . $caption . '</a>';
    }


    /**
     * Générateur de Modal bootstrap
     *
     * @param string $modal_id
     * @param string $modal_title
     * @param string $modal_body
     * @param string $modal_footer
     * @param string $modal_size
     * @return string
     */
    public static function viewModal(string $modal_id, string $modal_title, string $modal_body, string $modal_footer = '', string $modal_size = ''): string
    {
        if ($modal_size == 'lg') {
            $modal_size = ' modal-lg';
        } elseif ($modal_size == 'hg') {
            $modal_size = ' modal-hg';
        }
        return '<div class="modal fade" tabindex="-1" id="' . $modal_id . '" aria-hidden="true">
          <div class="modal-dialog' . $modal_size . '">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">' . $modal_title . '</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                ' . $modal_body . '
              </div>
              <div class="modal-footer">
                ' . $modal_footer . '
              </div>
            </div>
          </div>
        </div>';
    }

    /**
     * @param string $title
     * @param string $text
     * @param string $footer
     * @param string $style
     * @param string $class
     * @param string $id
     * @return string
     */
    public static function showCard(string $title, string $text, string $footer = '', string $style = '', string $class = '', string $id = ''): string
    {
        if ($footer) {
            $footer = '<div class="card-footer">' . $footer . '</div>';
        }
        return '<div class="card card-war ' . $class . '" style="' . $style . '" id="' . $id . '">
                <div class="card-header">' . $title . '</div>
                <div class="card-body">' . $text . '</div>
                ' . $footer . '
            </div>';
    }
}