<?php

const DB_HOST = 'localhost';
const DB_NAME = 'projetweb';
const DB_USER = 'root';
const DB_PASSWORD = '';
const ROOT_PATH = __DIR__;

// alternative syntax of constant declaration
define('ALTERNATIVE_CONST', 'test');